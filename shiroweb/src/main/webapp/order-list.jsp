<%--
  Created by IntelliJ IDEA.
  User: 和思良
  Date: 2021/1/2
  Time: 14:48
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html;charset=UTF-8" >
    <title>用户列表jsp页面</title>
    <style>
        table {border:1px solid #000000}
        table th{border:1px solid #000000}
        table td{border:1px solid #000000}
    </style>
</head>
<body>
<table cellpadding="0" cellspacing="0" width="80%">
    <tr>
        <th>编号</th>
        <th>公司名称</th>
        <th>信息来源</th>
        <th>所属行业</th>
        <th>级别</th>
        <th>联系地址</th>
        <th>联系电话</th>
    </tr>
    <tr>
        <th>1</th>
        <th>百度科技</th>
        <th>网络营销</th>
        <th>互联网</th>
        <th>VIP客户</th>
        <th>背景</th>
        <th>888888</th>
    </tr>
    <tr>
        <th>2</th>
        <th>阿里云</th>
        <th>网络营销</th>
        <th>互联网</th>
        <th>VIP客户</th>
        <th>背景</th>
        <th>888888</th>
    </tr>
    <tr>
        <th>3</th>
        <th>腾讯科技</th>
        <th>网络营销</th>
        <th>互联网</th>
        <th>VIP客户</th>
        <th>背景</th>
        <th>888888</th>
    </tr>



</table>

</body>
</html>
