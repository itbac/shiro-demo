package com.itbac.shiro.web;

import com.itbac.shiro.service.LoginService;
import com.itbac.shiro.service.impl.LoginServiceImpl;
import org.apache.shiro.authc.UsernamePasswordToken;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * @author: BacHe
 * @email: 1218585258@qq.com
 * @Date: 2021/1/2 14:08
 */
@WebServlet(urlPatterns = "/login")
public class LoginServlet extends HttpServlet {

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        //直接掉下面
        this.doPost(req, resp);
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
      //具体实现
        //1.获得用户名 密码
        String userName = req.getParameter("userName");
        String password = req.getParameter("password");
        //2.构建登录的token
        UsernamePasswordToken token = new UsernamePasswordToken(userName, password);
        //3.登录操作
        LoginService loginService = new LoginServiceImpl();
        boolean login = loginService.login(token);
        //3.1 登录成功，跳转首页 home.jsp
        if (login){
            //get Request Dispatcher 获取请求调度器
            //forward向前
            //跳转到 HomeServlet
            req.getRequestDispatcher("/home").forward(req, resp);
            return;
        }
        //3.2 登录失败，跳转登录页面 login.jsp
        //Redirect重定向
        resp.sendRedirect("login.jsp");








    }
}
