package com.itbac.shiro.service;

import org.apache.shiro.authc.UsernamePasswordToken;

/**
 * 登录服务
 * @author: BacHe
 * @email: 1218585258@qq.com
 * @Date: 2021/1/2 13:54
 */
public interface LoginService {

    //登录
    boolean login(UsernamePasswordToken token);

    //退出登录
    void logout();
}
