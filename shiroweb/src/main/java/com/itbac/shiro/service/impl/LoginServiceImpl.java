package com.itbac.shiro.service.impl;

import com.itbac.shiro.service.LoginService;
import org.apache.shiro.SecurityUtils;
import org.apache.shiro.authc.AuthenticationException;
import org.apache.shiro.authc.UsernamePasswordToken;
import org.apache.shiro.subject.Subject;

/**
 * @author: BacHe
 * @email: 1218585258@qq.com
 * @Date: 2021/1/2 13:55
 */
public class LoginServiceImpl implements LoginService {
    @Override
    public boolean login(UsernamePasswordToken token) {
        //获取主体
        Subject subject = SecurityUtils.getSubject();
        try {
            //操作登录
            subject.login(token);
        } catch (AuthenticationException e) {
            e.printStackTrace();
            //登录异常，则失败。
            return false;
        }
        //是否登录成功
        return subject.isAuthenticated();
    }

    @Override
    public void logout() {
        //获取主体
        Subject subject = SecurityUtils.getSubject();
        //退出登录
        subject.logout();
    }
}
