package com.itbac.shiro.service;

import java.util.List;
import java.util.Map;

/**
 * @author: BacHe
 * @email: 1218585258@qq.com
 * @Date: 2020/12/27 16:44
 */
public interface SecurityService {

    /**
     * 根据用户名获取 密码 和 盐
     */
    Map<String,String> getPasswordByUserName(String userName);
    /**
     * 查询角色列表
     */
    List<String> findRoleByLoginName(String loginName);
    /**
     * 查询权限列表
     */
    List<String> findPermissionByLoginName(String loginName);

}
