package utils;

import org.apache.shiro.crypto.SecureRandomNumberGenerator;
import org.apache.shiro.crypto.hash.SimpleHash;

import java.util.HashMap;
import java.util.Map;

/**
 * 生成摘要
 * 加密解密
 */
public class DigestsUtil {
    //加密算法 ：SHA-1、SHA-224、SHA-256、SHA-384,和SHA-512
    public static final String SHA1 = "SHA-256";
    //密码迭代次数
    public static final Integer ITBRATIONS = 512;

    /**
     * @description 数据加密
     * @param input 明文
     * @param salt 盐
     * @return 秘文
     */
    public static String sha1(String input,String salt){
        return new SimpleHash(SHA1, input, salt, ITBRATIONS).toString();
    }
    //生成随机盐
    public static String generateSalt(){
        SecureRandomNumberGenerator randomNumberGenerator = new SecureRandomNumberGenerator();
        return randomNumberGenerator.nextBytes().toHex();
    }
    //生成获取 密文 和 盐
    public static Map<String,String> entryptPassword(String input){
        Map<String, String> map = new HashMap<>();
        String salt = generateSalt();
        String password = sha1(input, salt);
        map.put("salt", salt);
        map.put("password", password);
        return map;
    }

}
