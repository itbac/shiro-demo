package demo1;

import org.apache.shiro.SecurityUtils;
import org.apache.shiro.authc.UsernamePasswordToken;
import org.apache.shiro.config.IniSecurityManagerFactory;
import org.apache.shiro.mgt.SecurityManager;
import org.apache.shiro.subject.Subject;
import org.apache.shiro.util.Factory;

/**
 * 直接读取ini配置文件，获取账号密码
 * @author: BacHe
 * @email: 1218585258@qq.com
 * @Date: 2020/12/27 16:21
 */
public class demo1 {
    public static void main(String[] args) {
        //导入权限ini文件构建权限工厂
        Factory<SecurityManager> factory = new IniSecurityManagerFactory("classpath:shiro.ini");
        //工厂构建安全管理器
        SecurityManager securityManager = factory.getInstance();
        //使用工具生效安全管理器
        SecurityUtils.setSecurityManager(securityManager);
        //获取主体
        Subject subject = SecurityUtils.getSubject();
        //构建账号token
        UsernamePasswordToken token = new UsernamePasswordToken("bac", "123456");
        //登录操作
        subject.login(token);
        //校验
        System.out.println("是否登录成功：" + subject.isAuthenticated());


    }
}
