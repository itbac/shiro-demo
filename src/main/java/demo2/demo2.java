package demo2;

import org.apache.shiro.SecurityUtils;
import org.apache.shiro.authc.UsernamePasswordToken;
import org.apache.shiro.authz.AuthorizationException;
import org.apache.shiro.config.IniSecurityManagerFactory;
import org.apache.shiro.mgt.SecurityManager;
import org.apache.shiro.subject.Subject;
import org.apache.shiro.util.Factory;
import org.junit.Test;

/**
 * 直接读取ini配置文件，获取账号密码
 * @author: BacHe
 * @email: 1218585258@qq.com
 * @Date: 2020/12/27 16:21
 */
public class demo2 {

    //登录方法，返回主体
    private Subject login() {
        //导入权限ini文件构建权限工厂
        Factory<SecurityManager> factory = new IniSecurityManagerFactory("classpath:shiro2.ini");
        //工厂构建安全管理器
        SecurityManager securityManager = factory.getInstance();
        //使用工具生效安全管理器
        SecurityUtils.setSecurityManager(securityManager);
        //获取主体
        Subject subject = SecurityUtils.getSubject();
        //构建账号token
        UsernamePasswordToken token = new UsernamePasswordToken("bac", "123456");
        //登录操作 ,调试，会走到 MyRealm 类的 doGetAuthenticationInfo（） 认证方法
        subject.login(token);
        //校验
        System.out.println("是否登录成功：" + subject.isAuthenticated());
        //返回登录主体
        return subject;
    }
    //测试 角色与权限校验
    @Test
    public void  test(){
        //登录
        Subject subject = login();
        //校验当前用户，是否有管理员admin角色
        System.out.println(String.format("当前账号[%s][%s]管理员admin角色",
                subject.getPrincipal(),subject.hasRole("admin")?"有":"没有"));
        //校验一个用户没有的角色，会抛异常
        try {
            //调试，会走到 MyRealm 类的 doGetAuthorizationInfo（） 鉴权方法
            subject.checkRole("coder");
            System.out.println("有角色,coder");
        } catch (AuthorizationException e) {
            System.out.println("没有角色,coder");
        }
        //校验当前用户的权限信息
        System.out.println(String.format("当前账号[%s][%s]权限 order:add",
                subject.getPrincipal(),subject.isPermitted("order:add")?"有":"没有"));
        //校验当前用户没有的权限信息
        try {
            subject.checkPermission("user.add");
            System.out.println("有权限,user.add");
        } catch (AuthorizationException e) {
            System.out.println("没有权限,user.add");
        }


    }

}
