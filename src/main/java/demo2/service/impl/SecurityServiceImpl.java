package demo2.service.impl;

import demo2.service.SecurityService;
import utils.DigestsUtil;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * 模拟查询数据库
 * @author: BacHe
 * @email: 1218585258@qq.com
 * @Date: 2020/12/27 16:45
 */
public class SecurityServiceImpl implements SecurityService {
    @Override
    public Map<String,String> getPasswordByUserName(String userName) {
        if ("bac".equals(userName)) {
            return DigestsUtil.entryptPassword("123456");
        }
        return null;
    }

    @Override
    public List<String> findRoleByLoginName(String loginName) {
        List<String> roles = new ArrayList<>();
        roles.add("admin");
        roles.add("dev");
        return roles;
    }

    @Override
    public List<String> findPermissionByLoginName(String loginName) {
        List<String> list = new ArrayList<>();
        //资源 order , 操作：增删改查
        //order:*  表示全部 （ @see WildcardPermission）
        list.add("order:add");
        list.add("order:del");
        list.add("order:update");
        list.add("order:list");
        return list;
    }
}
