import utils.DigestsUtil;

import java.util.Map;

/**
 * @author: BacHe
 * @email: 1218585258@qq.com
 * @Date: 2020/12/27 23:55
 */
public class entryptTest {
    public static void main(String[] args) {
        Map<String, String> map = DigestsUtil.entryptPassword("123456");
        //{password=652ea64b8858d3b50c693196772c8b7e1c067f56, salt=d2c09f151a4a5b0ff4c6c191d27b2abd}
        System.out.println(map);
    }
}
